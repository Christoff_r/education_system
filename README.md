# Education System :school:

![banner](https://cdn.dribbble.com/users/988448/screenshots/5240042/media/9453a27e8dbaac2f2bda2449711e585a.gif)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![language](https://img.shields.io/badge/language-c%2B%2B-blue)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [License](#license)


## Background

The "Edutation System" allows you to create a course, teacher and student with corresponding information about them. Next you can look for specific courses, teacher or student to view this information using the ID you gave it/them, see [usage](#usage) for more details.

## Install
Below is a short description on how to install the program.

### Dependencies

The program is compiled using [CMake](https://cmake.org/install/) and need to be installed first.

### Clone from GitLab

Navigate to were you want the repository to be. Open the `CLI` and type the following:

```
git clone https://gitlab.com/Christoff_r/education_system.git
```

## Usage

The program need to compile first.

### Compile

In the repository, open the `CLI` and type the following. 

```
cd build
cmake ../
make
```
### Run

To run the program open the `CLI` in the prepository (if you are not still in the build folder from the last step) and type the following

```
cd build
./main
```
### Interact

You will be promted with a long [list of commands](#list-of-commands) of what you can do.

For eksample to create a course wirte the following
```
create_course
```
Then press enter and follow the onscreen promt for filling in additinal infomation about the course.

### List of commands

- 'create_course' - Make a new course
- 'create_teacher' - Make a new teacher
- 'create_student' - Make a new student
- 'look_course' - See info about a specific course
- 'look_teacher' - See info about a specific teacher
- 'look_student' - See info about a specific student
- 'add_teacher' - Add teacher to a course
- 'add_student' - Add student to a course
- 'drop_teacher' - Remove teacher to a course
- 'drop_student' - Remove student to a course
- 'quit' - quits the program

### Warning 

There is currently no savegurds for user input. For eksample if you enter a string insted of an int as an ID the program will exploed and quit. Likewise if you enter the same ID for two students/teachers/coourses you will only be able to use the first one with that ID.

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`