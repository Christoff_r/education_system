#include "course.h"
#include "teacher.h"
#include "student.h"
#include <string>
using namespace std;

Course::Course()
{
    
}

Course::Course(string courseName, int id)
{
    this->courseName = courseName;
    this->id = id;
}

string Course::GetName()
{
    return courseName;
}

int Course::GetId()
{
    return id;
}