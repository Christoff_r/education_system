#pragma once
#include <string>
#include "person.h"
using namespace std;

class Student : public Person
{
    public:
        Student();
        Student(string firsName, string lastName, int id);

    // Save
    template<class B>
    void serialize(B& buf) const
    {
        buf << firstName << lastName << id;
    }

    // Load
    template<class B>
    void parse(B& buf)
    {
        buf >> firstName >> lastName >> id;
    }   
};