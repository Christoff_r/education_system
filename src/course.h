#pragma once
#include <string>
#include <vector>
#include "teacher.h"
#include "student.h"
using namespace std;

class Course
{
    protected:
        string courseName;
        int id;
    public:
        Course();
        Course(string courseName, int id);
        string GetName();
        int GetId();

    // Save
    template<class B>
    void serialize(B& buf) const
    {
        buf << courseName << id;
    }

    // Load
    template<class B>
    void parse(B& buf)
    {
        buf >> courseName >> id;
    } 
};