#pragma once
#include <string>
using namespace std;

class Person
{
  protected:
  	string firstName;
  	string lastName;
    int id;
  public:
    Person();
  	Person(string firstName, string lastName, int id);
    string GetName();
    int GetId();
};