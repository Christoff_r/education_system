#include "person.h"
#include "../hps/src/hps.h"
#include <string>
using namespace std;

Person::Person()
{
	
}

Person::Person(string firstName, string lastName, int id)
{
  		this->firstName = firstName;
  		this->lastName = lastName;
		this->id = id;
}

string Person::GetName()
{
    return firstName + " " + lastName;
}

int Person::GetId()
{
	return id;
}