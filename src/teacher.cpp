#include "teacher.h"
#include "person.h"
#include <string>
using namespace std;

Teacher::Teacher()
{
    
}

Teacher::Teacher(string firstName, string lastName, int id, int salary)
:Person(firstName, lastName, id)
{
    this->salary = salary;
}

int Teacher::GetSalary()
{
    return salary;
}