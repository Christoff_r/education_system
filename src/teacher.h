#pragma once
#include <string>
#include "person.h"
using namespace std;

class Teacher : public Person
{
    protected:
        int salary;
    public:
        Teacher();
        Teacher(string firstName, string lastName, int id, int salary);
        int GetSalary();

    // Save
    template<class B>
    void serialize(B& buf) const
    {
        buf << firstName << lastName << id << salary;
    }

    // Load
    template<class B>
    void parse(B& buf)
    {
        buf >> firstName >> lastName >> id >> salary;
    } 
};