#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "inputHandler.h"
#include "course.h"
#include "person.h"
#include "teacher.h"
#include "student.h"
#include "../hps/src/hps.h"

using namespace std;

string input;
bool startUp = true;

vector<Course> coursesVector;
vector<Teacher> teachersVector;
vector<Student> studentsVector;
vector<pair <string, string> > courseTeacherVector; 
vector<pair <string, string> > courseStudentVector; 

void save()
{
        cout << "Quitting..." << endl;
        // Save to file
        cout << "[----------]" << endl;
        auto out_stream_student = ofstream("../saves/students.txt", ios::binary);
        hps::to_stream(studentsVector, out_stream_student);
        out_stream_student.close();

        cout << "[##--------]" << endl;
        auto out_stream_teacher = ofstream("../saves/teachers.txt", ios::binary);
        hps::to_stream(teachersVector, out_stream_teacher);
        out_stream_teacher.close();

        cout << "[####------]" << endl;
        auto out_stream_course = ofstream("../saves/courses.txt", ios::binary);
        hps::to_stream(coursesVector, out_stream_course);
        out_stream_course.close();

        cout << "[######----]" << endl;
        auto out_stream_ct = ofstream("../saves/courseTeacher.txt", ios::binary);
        hps::to_stream(courseTeacherVector, out_stream_ct);
        out_stream_ct.close();

        cout << "[########--]" << endl;
        auto out_stream_cs = ofstream("../saves/courseStudent.txt", ios::binary);
        hps::to_stream(courseStudentVector, out_stream_cs);
        out_stream_cs.close();
        cout << "[##########]" << endl;
}

void load()
{

    auto in_stream_student = ifstream("../saves/students.txt", ios::binary);
    studentsVector = hps::from_stream<vector<Student> >(in_stream_student);
    in_stream_student.close();

    auto in_stream_teacher = ifstream("../saves/teachers.txt", ios::binary);
    teachersVector = hps::from_stream<vector<Teacher> >(in_stream_teacher);
    in_stream_teacher.close();

    auto in_stream_course = ifstream("../saves/courses.txt", ios::binary);
    coursesVector = hps::from_stream<vector<Course> >(in_stream_course);
    in_stream_course.close();
    
    auto in_stream_cs = ifstream("../saves/courseStudent.txt", ios::binary);
    courseStudentVector = hps::from_stream<vector<pair<string,string>> >(in_stream_cs);
    in_stream_cs.close();
    
    auto in_stream_ct = ifstream("../saves/courseTeacher.txt", ios::binary);
    courseTeacherVector = hps::from_stream<vector<pair<string,string>> >(in_stream_ct);
    in_stream_ct.close();
}

void menu()
{
    if (startUp)
    {
        load();
        startUp = false;
    }

    cout << "Hello! Pleas enter whether you want to \n 'create_course', 'create_teacher', 'create_student' \n 'add_teacher', 'add_student' \n 'look_course', 'look_teacher, 'look_student' \n 'drop_teacher', 'drop_student' \n or wants to 'quit'" << endl;
    cin >> input;

    if (input == "create_course")
    {
        string courseName;
        int id;

        cout << "Enter course name: " << endl;
        cin >> courseName;
        cout << "Enter course ID: " << endl;
        cin >> id;

        Course course = Course(courseName, id);
        coursesVector.push_back(course);
        cout << "You created a course named " << course.GetName() << endl;
        
        menu();
    }
    else if (input == "create_teacher")
    {
        string firstName;
        string lastName;
        int id;
        int salary;

        cout << "Enter teacher first Name" << endl;
        cin >> firstName;
        cout << "Enter teacher last Name" << endl;
        cin >> lastName;
        cout << "Enter teacher ID" << endl;
        cin >> id;
        cout << "Enter teacher salary" << endl;
        cin >> salary;

        Teacher teacher = Teacher(firstName, lastName, id, salary);
        teachersVector.push_back(teacher);
        cout << "You created a teacher named " << teacher.GetName() << endl;
        
        menu();
    }
    else if (input == "create_student")
    {
        string firstName;
        string lastName;
        int id;

        cout << "Enter student first Name" << endl;
        cin >> firstName;
        cout << "Enter student last Name" << endl;
        cin >> lastName;
        cout << "Enter students ID" << endl;
        cin >> id;
        
        Student student = Student(firstName, lastName, id);
        studentsVector.push_back(student);
        cout << "You created a student named " << student.GetName() << endl;


        menu();
    }
    else if (input == "look_course")
    {
        int id;

        cout << "Enter course id: " << endl;
        cin >> id;

        for (auto course : coursesVector)
        {
            if(course.GetId() == id)
            {
                cout << "-------INFO-------" << endl;
                cout << "Name: " << course.GetName() << endl;
                cout << "ID: " << course.GetId() << endl;
                cout << "Teacher(s): " << endl;
                for (auto pair: courseTeacherVector)
                {
                    if(pair.first == course.GetName())
                        cout << pair.second << endl;
                }
                cout << "Student(s): " << endl;
                for (auto pair: courseStudentVector)
                {
                    if(pair.first == course.GetName())
                        cout << pair.second << endl;
                }
                
            }
        }

        menu();
    }
    else if (input == "look_teacher")
    {
        int id;

        cout << "Enter teacher id: " << endl;
        cin >> id;

        for (auto i : teachersVector)
        {
            if(i.GetId() == id)
            {
                cout << "-------INFO-------" << endl;
                cout << "Name: " << i.GetName() << endl;
                cout << "ID: " << i.GetId() << endl;
                cout << "Salary: " << i.GetSalary() << endl;
                break;
            }
        }

        menu();
    }
    else if (input == "look_student")
    {
        int id;

        cout << "Enter student id: " << endl;
        cin >> id;

        for (auto i : studentsVector)
        {
            if(i.GetId() == id)
            {
                cout << "-------INFO-------" << endl;
                cout << "Name: " << i.GetName() << endl;
                cout << "ID: " << i.GetId()<< endl;
                break;
            }
        }

        menu();
    }
    else if (input == "add_teacher")
    {
        int teacherID;
        int courseID;

        cout << "Enter teacher ID" << endl;
        cin >> teacherID;
        cout << "Enter course ID" << endl;
        cin >> courseID;

        for (auto course : coursesVector)
        {
            if(course.GetId() == courseID)
            {
                for (auto teacher : teachersVector)
                {
                    if(teacher.GetId() == teacherID)
                    {
                        string courseName = course.GetName();
                        string teacherName = teacher.GetName();
                        courseTeacherVector.push_back(make_pair(courseName, teacherName));
                        break;
                    }
                }
            }
            break;
        }

        menu();
    }
    else if (input == "add_student")
    {
        int studentID;
        int courseID;

        cout << "Enter student ID" << endl;
        cin >> studentID;
        cout << "Enter course ID" << endl;
        cin >> courseID;

        for (auto course : coursesVector)
        {
            if(course.GetId() == courseID)
            {
                for (auto student : studentsVector)
                {
                    if(student.GetId() == studentID)
                    {
                        string courseName = course.GetName();
                        string studentName = student.GetName();
                        courseStudentVector.push_back(make_pair(courseName, studentName));
                        break;
                    }
                }
            }
            break;
        }

        menu();
    }
    else if (input == "drop_teacher")
    {
        int teacherID;
        int courseID;
        string teacherName;
        string courseName;

        cout << "Enter teacher ID" << endl;
        cin >> teacherID;
        cout << "Enter course ID" << endl;
        cin >> courseID;

        for (auto teacher : teachersVector)
            if(teacher.GetId() == teacherID)
                teacherName = teacher.GetName();
        
        for (auto course : coursesVector)
            if(course.GetId() == courseID)
                courseName = course.GetName();

        for (int i = 0; i < courseTeacherVector.size(); i++)
        {
            if(courseTeacherVector[i].first == courseName && courseTeacherVector[i].second == teacherName)
                courseTeacherVector.erase(courseTeacherVector.begin() + i);
        }

        menu();
    }
    else if (input == "drop_student")
    {
        int studentID;
        int courseID;
        string studentName;
        string courseName;

        cout << "Enter student ID" << endl;
        cin >> studentID;
        cout << "Enter course ID" << endl;
        cin >> courseID;

        for (auto student : studentsVector)
            if(student.GetId() == studentID)
                studentName = student.GetName();
        
        for (auto course : coursesVector)
            if(course.GetId() == courseID)
                courseName = course.GetName();

        for (int i = 0; i < courseStudentVector.size(); i++)
        {
            if(courseStudentVector[i].first == courseName && courseStudentVector[i].second == studentName)
                courseStudentVector.erase(courseStudentVector.begin() + i);
        }

        menu();
    }
    else if (input == "quit")
    {
        save();
    }
    else
    {
        cout << "Command not understood?" << endl;
        menu();
    } 
}